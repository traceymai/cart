<?php
session_start();
class Item {
    public $name;
    public $price;
    public $quantity;

    function __construct($name, $price, $quantity) {
        $this->name=$name;
        $this->price=number_format($price, 2, ".", "");
        $this->quantity = $quantity;
    }

    function get_price() {
        return $this->price;
    }

    function get_name() {
        return $this->name;
    }

    function get_current_quantity() {
        return $this->quantity;
    }

    function get_total_per_item() {
        return number_format($this->price * $this->quantity, 2, '.', '');
    }

    function display_item_as_str() {
        echo "Item Name: ".$this->name." ; Quantity: ".$this->quantity." ; Total for this item: ".$this->get_total_per_item()."<br>";
    }

    function display_item_in_shelf() {
        echo "Item Name: ".$this->name." ; Price: ".$this->price. "<br>";

    }
    
    function add_item($new_quantity) {
        $this->quantity = $this->quantity + $new_quantity;
    }

    function remove_item($quant_to_remove) {
        if ($quant_to_remove < $this->get_current_quantity()) {
            $this->quantity -= $quant_to_remove;
        } elseif ($quant_to_remove == $this->get_current_quantity()) {

        } 
    }
}

class Shelf {
    public $all_prods_list = array();
    function __construct($products) {
        foreach($products as $each_prod) {
            array_push($this->all_prods_list, new Item($each_prod["name"], $each_prod["price"], 0));
        }
    }
    function get_all_prods_list() {
        return $this->all_prods_list;
    }
}
class ShoppingCart {
    public $itemlist = array(); // this is a key-value array where 
    // key = item name; value = item quantity

    function __construct($itemlist) {
        $this->itemlist = $itemlist;
    }

    function check_if_item_exists($itemname) {
        foreach($this->itemlist as $i => $indiv_item) {
            if ($indiv_item->get_name() === $itemname) {
                return $i;
            }
        }
        return -1;
    }

    function get_total_per_cart() {
        $cart_total = 0;
        foreach($this->itemlist as $indiv_item) {
            $cart_total += $indiv_item.get_total_per_item();
        }
        return number_format($cart_total, 2, '.', '');
    }

    function add_item_to_cart($productName, $productPrice, $quantity) {
        $msg="Successfully added item ";
        // input validation
        if(($quantity > 0) && ( filter_var($quantity, FILTER_VALIDATE_INT) == true )) {
            // if the item already exists in the cart, we just add $quantity to the quantity of that item in cart
            if ($this->check_if_item_exists($productName) !== -1) {
                $this->itemlist[$this->check_if_item_exists($productName)]->add_item($quantity);
            }
            else {
                // if item's not already in the cart (adding a new item with quantity)
                array_push($this->itemlist, new Item($productName, $productPrice, $quantity));
            }
            $msg.=$productName;
        }
        
        else {
            $msg="Please make sure quantity is a positive integer.";
        }
        return $msg;
    }
    function remove_item_from_cart($productName, $productPrice, $currentQuantity,
    $quantityDel) {
        $msg="Successfully removed item ";
        // input validation
        // // valid input
        if(($quantityDel > 0) && ( filter_var($quantityDel, FILTER_VALIDATE_INT) == true )
        && ($quantityDel <= $currentQuantity)) {
            // if there's still at least 1 item of certain product in the cart
            // after removal
            if ($quantityDel < $currentQuantity) {
                // return index of item in cart (we know this will return a valid index not equal to -1)
                $idx = $this->check_if_item_exists($productName);
                $this->itemlist[$idx]->quantity -= $quantityDel;
            }
            elseif ($quantityDel == $currentQuantity) {
                // if trying to remove all products with given name in cart
                $idx = $this->check_if_item_exists($productName);
                // remove all items of said name and reindex
                unset($this->itemlist[$idx]);
                $this->itemlist = array_values($this->itemlist);

            }
            $msg.=$productName;
        }
        else {
            $msg="Please make sure:<br>
            1) Quantity to delete is a positive integer<br>
            2) Quantity to delete is at most quantity of product in the cart<br>";
        }
        return $msg;
    }
    

    function view_current_products() {
        return $this->itemlist;
    }
}
if(!(isset($_SESSION['cart']))) {
    echo "true\n";
    $_SESSION['cart'] = new ShoppingCart(array());
}

$msg="";
// adding products
if(isset($_GET["productName"])) {
    $productName = $_GET['productName'];
    $productPrice = $_GET['price'];
    $quantity = $_GET["quantity"];


    $msg = $_SESSION['cart']->add_item_to_cart($productName, $productPrice, $quantity);
    // echo "<pre>".$msg."</pre>";
}

// removing products
if(isset($_GET["productName_del"])) {
    $productName_del = $_GET['productName_del'];
    $productPrice_del = $_GET['price_del'];
    $quantity_del = $_GET["quantity_del"];
    $current_quantity = $_GET["current_quantity"];


    $msg = $_SESSION['cart']->remove_item_from_cart($productName_del, $productPrice_del, $current_quantity, $quantity_del);
    // echo "<pre>".$msg."</pre>";
}
// echo "<pre>";
// print_r($_SESSION['cart']);
// echo "</pre>";
// echo "<pre>";
// $_SESSION["cart"]->view_current_products();
// echo "</pre>";
$products = $products = [
    [ "name" => "Sledgehammer", "price" => 125.75 ],
    [ "name" => "Axe", "price" => 190.50 ],
    [ "name" => "Bandsaw", "price" => 562.131 ],
    [ "name" => "Chisel", "price" => 12.9 ],
    [ "name" => "Hacksaw", "price" => 18.45 ],
    ];



?>

<html>
    <head>
    <title>ezyVet's Developer Practical Test</title>
    </head>

    <body>
        <h1>Tracey Mai's PHP Shopping Cart</h1>
        <?php
            echo $msg;
            echo "<h2>Available Items on the Shelf</h2>";
            $shelf = new Shelf($products);
            echo "<table><tr><th>Product Name</th><th>Price</th><th>Quantity Desired</th></tr>";
            foreach($shelf->get_all_prods_list() as $each_item) {
                /*$each_item->display_item_in_shelf();*/
                echo "<tr><td>".$each_item->get_name()."</td>";
                echo "<td>".$each_item->get_price()."</td>";
                echo "<td><form action='{$_SERVER['PHP_SELF']}'>
                    <input type='text' name='quantity' id='quantity'>
                    <input type='hidden' name='price' id='price' value=".$each_item->get_price().">".
                    "<input type='hidden' name='productName' id='productName' value=".$each_item->get_name().">".
                    "<input type='submit' value='add'>
                    </form></td></tr>";
            }
            echo "</table>";
            echo str_repeat("<br>", 3);


            echo "<h2>Available Items in Your Cart</h2>";
            $current_cart = $_SESSION["cart"];
            $cart_total = 0;
            echo "<table><tr><th>Product Name</th><th>Price</th><th>Quantity</th><th>Total</th><th>Quantity to Remove</th></tr>";
            foreach($current_cart->itemlist as $i => $current_item) {
                echo "<tr><td>".$current_item->get_name()."</td>";
                echo "<td>".$current_item->get_price()."</td>";
                echo "<td>".$current_item->get_current_quantity()."</td>";
                echo "<td>".($current_item->get_total_per_item())."</td>";
                $cart_total+=$current_item->get_total_per_item();
                echo "<td><form action='{$_SERVER['PHP_SELF']}'>
                    <input type='text' name='quantity_del' id='quantity_del'>
                    <input type='hidden' name='current_quantity' id='current_quantity' value=".$current_item->get_current_quantity().">".
                    "<input type='hidden' name='productName_del' id='productName_del' value=".$current_item->get_name().">".
                    "<input type='hidden' name='price_del' id='price_del' value=".$current_item->get_price().">".
                    "<input type='submit' value='remove'>
                    </form></td></tr>";
            }
            echo "</table>";
            echo "<p><strong>Cart Total: </strong>".$cart_total."<br";
        ?>
    </body>

</html>